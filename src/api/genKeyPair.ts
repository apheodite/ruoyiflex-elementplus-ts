import request from '@/utils/request';

/**
 * 生成密钥对
 * @param callback
 */
export function genKeyPair(callback: Function) {
  return request({
    url: '/genKeyPair',
    headers: {
      isToken: false,
      repeatSubmit: false
    },
    method: 'get'
  }).then((res) => {
    return callback(res.data.uuidPrivateKey, res.data.RSA_PUBLIC_KEY);
  });
}
