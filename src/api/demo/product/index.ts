import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { DemoProductVO, DemoProductForm, DemoProductQuery } from '@/api/demo/product/types';

/**
 * 查询产品树列表
 * @param query
 * @returns {*}
 */

export const listProduct = (query?: DemoProductQuery): AxiosPromise<DemoProductVO[]> => {
  return request({
    url: '/demo/product/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询产品树详细
 * @param productId
 */
export const getProduct = (productId: string | number): AxiosPromise<DemoProductVO> => {
  return request({
    url: '/demo/product/' + productId,
    method: 'get'
  });
};

/**
 * 新增产品树
 * @param data
 */
export const addProduct = (data: DemoProductForm) => {
  return request({
    url: '/demo/product',
    method: 'post',
    data: data
  });
};

/**
 * 修改产品树
 * @param data
 */
export const updateProduct = (data: DemoProductForm) => {
  return request({
    url: '/demo/product',
    method: 'put',
    data: data
  });
};

/**
 * 删除产品树
 * @param productId
 */
export const delProduct = (productId: string | number | Array<string | number>) => {
  return request({
    url: '/demo/product/' + productId,
    method: 'delete'
  });
};
