import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { DemoCustomerVO, DemoCustomerForm, DemoCustomerQuery } from '@/api/demo/customer/types';

/**
 * 查询客户主表列表
 * @param query
 * @returns {*}
 */

export const listCustomer = (query?: DemoCustomerQuery): AxiosPromise<DemoCustomerVO[]> => {
  return request({
    url: '/demo/customer/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询客户主表详细
 * @param customerId
 */
export const getCustomer = (customerId: string | number): AxiosPromise<DemoCustomerVO> => {
  return request({
    url: '/demo/customer/' + customerId,
    method: 'get'
  });
};

/**
 * 新增客户主表
 * @param data
 */
export const addCustomer = (data: DemoCustomerForm) => {
  return request({
    url: '/demo/customer',
    method: 'post',
    data: data
  });
};

/**
 * 修改客户主表
 * @param data
 */
export const updateCustomer = (data: DemoCustomerForm) => {
  return request({
    url: '/demo/customer',
    method: 'put',
    data: data
  });
};

/**
 * 删除客户主表
 * @param customerId
 */
export const delCustomer = (customerId: string | number | Array<string | number>) => {
  return request({
    url: '/demo/customer/' + customerId,
    method: 'delete'
  });
};
