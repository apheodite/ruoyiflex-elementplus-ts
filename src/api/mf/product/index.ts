import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { ProductVO, ProductForm, ProductQuery } from '@/api/mf/product/types';

/**
 * 查询产品树列表
 * @param query
 * @returns {*}
 */

export const listProduct = (query?: ProductQuery): AxiosPromise<ProductVO[]> => {
  return request({
    url: '/mf/product/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询产品树详细
 * @param productId
 */
export const getProduct = (productId: string | number): AxiosPromise<ProductVO> => {
  return request({
    url: '/mf/product/' + productId,
    method: 'get'
  });
};

/**
 * 新增产品树
 * @param data
 */
export const addProduct = (data: ProductForm) => {
  return request({
    url: '/mf/product',
    method: 'post',
    data: data
  });
};

/**
 * 修改产品树
 * @param data
 */
export const updateProduct = (data: ProductForm) => {
  return request({
    url: '/mf/product',
    method: 'put',
    data: data
  });
};

/**
 * 删除产品树
 * @param productId
 */
export const delProduct = (productId: string | number | Array<string | number>) => {
  return request({
    url: '/mf/product/' + productId,
    method: 'delete'
  });
};
