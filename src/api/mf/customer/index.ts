import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { CustomerVO, CustomerForm, CustomerQuery } from '@/api/mf/customer/types';

/**
 * 查询客户主表列表
 * @param query
 * @returns {*}
 */

export const listCustomer = (query?: CustomerQuery): AxiosPromise<CustomerVO[]> => {
  return request({
    url: '/mf/customer/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询客户主表详细
 * @param customerId
 */
export const getCustomer = (customerId: string | number): AxiosPromise<CustomerVO> => {
  return request({
    url: '/mf/customer/' + customerId,
    method: 'get'
  });
};

/**
 * 新增客户主表
 * @param data
 */
export const addCustomer = (data: CustomerForm) => {
  return request({
    url: '/mf/customer',
    method: 'post',
    data: data
  });
};

/**
 * 修改客户主表
 * @param data
 */
export const updateCustomer = (data: CustomerForm) => {
  return request({
    url: '/mf/customer',
    method: 'put',
    data: data
  });
};

/**
 * 删除客户主表
 * @param customerId
 */
export const delCustomer = (customerId: string | number | Array<string | number>) => {
  return request({
    url: '/mf/customer/' + customerId,
    method: 'delete'
  });
};
