import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { StudentVO, StudentForm, StudentQuery } from '@/api/mf/student/types';

/**
 * 查询学生信息表列表
 * @param query
 * @returns {*}
 */

export const listStudent = (query?: StudentQuery): AxiosPromise<StudentVO[]> => {
  return request({
    url: '/mf/student/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询学生信息表详细
 * @param studentId
 */
export const getStudent = (studentId: string | number): AxiosPromise<StudentVO> => {
  return request({
    url: '/mf/student/' + studentId,
    method: 'get'
  });
};

/**
 * 新增学生信息表
 * @param data
 */
export const addStudent = (data: StudentForm) => {
  return request({
    url: '/mf/student',
    method: 'post',
    data: data
  });
};

/**
 * 修改学生信息表
 * @param data
 */
export const updateStudent = (data: StudentForm) => {
  return request({
    url: '/mf/student',
    method: 'put',
    data: data
  });
};

/**
 * 删除学生信息表
 * @param studentId
 */
export const delStudent = (studentId: string | number | Array<string | number>) => {
  return request({
    url: '/mf/student/' + studentId,
    method: 'delete'
  });
};
