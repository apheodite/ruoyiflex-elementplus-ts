export interface StudentVO extends BaseEntity {
    /**
    * 编号
    */
        studentId: string | number;

    /**
    * 学生名称
    */
        studentName: string;

    /**
    * 年龄
    */
        studentAge: number;

    /**
    * 爱好（0代码 1音乐 2电影）
    */
        studentHobby: string;

    /**
    * 性别（1男 2女 3未知）
    */
        studentGender: string;

    /**
    * 状态（0正常 1停用）
    */
        studentStatus: string;

    /**
    * 生日
    */
        studentBirthday: string;

    }

    export interface StudentForm {
        /**
        * 编号
        */
        studentId?: string | number;

        /**
        * 学生名称
        */
        studentName?: string;

        /**
        * 年龄
        */
        studentAge?: number;

        /**
        * 爱好（0代码 1音乐 2电影）
        */
        studentHobby?: string;

        /**
        * 性别（1男 2女 3未知）
        */
        studentGender?: string;

        /**
        * 状态（0正常 1停用）
        */
        studentStatus?: string;

        /**
        * 生日
        */
        studentBirthday?: string;

        /**
        * 乐观锁
        */
        version?: number;

    }

    export interface StudentQuery extends PageQuery {
        /**
        * 学生名称
        */
        studentName?: string;

        /**
        * 状态（0正常 1停用）
        */
        studentStatus?: string;

    /**
    * 日期范围参数
    */
    params?: any;
    }
